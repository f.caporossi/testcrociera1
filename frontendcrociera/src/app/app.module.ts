import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {FormsModule} from '@angular/forms';
import {ReactiveFormsModule} from '@angular/forms';
import {MatSidenavModule} from '@angular/material/sidenav';


import { AppComponent } from './app.component';
import { RegisterComponent } from './pages/register/register.component';
import { RegistrazioneclienteComponent } from './pages/registrazionecliente/registrazionecliente.component';
import { PrenotazionecabinaComponent } from './pages/prenotazionecabina/prenotazionecabina.component';
import { PrenotazioneescursioneComponent } from './pages/prenotazioneescursione/prenotazioneescursione.component';
import { PrenotazioneeventoComponent } from './pages/prenotazioneevento/prenotazioneevento.component';
import { HomeComponent } from './pages/home/home.component';

import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import { ContactComponent } from './pages/contact/contact.component';
import { AboutComponent } from './pages/about/about.component';
import { LoginComponent } from './pages/login/login.component';
import { HeaderComponent } from './core/header/header.component';
import { FooterComponent } from './core/footer/footer.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';


@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    HomeComponent,
    RegistrazioneclienteComponent,
    PrenotazionecabinaComponent,
    PrenotazioneescursioneComponent,
    PrenotazioneeventoComponent,
    ContactComponent,
    AboutComponent,
    LoginComponent,
    FooterComponent,
    HeaderComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
