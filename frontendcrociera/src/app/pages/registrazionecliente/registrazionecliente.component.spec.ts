import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrazioneclienteComponent } from './registrazionecliente.component';

describe('RegistrazioneclienteComponent', () => {
  let component: RegistrazioneclienteComponent;
  let fixture: ComponentFixture<RegistrazioneclienteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegistrazioneclienteComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RegistrazioneclienteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
