import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrenotazionecabinaComponent } from './prenotazionecabina.component';

describe('PrenotazionecabinaComponent', () => {
  let component: PrenotazionecabinaComponent;
  let fixture: ComponentFixture<PrenotazionecabinaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrenotazionecabinaComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PrenotazionecabinaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
