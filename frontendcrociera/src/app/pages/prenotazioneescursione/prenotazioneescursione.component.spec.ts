import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrenotazioneescursioneComponent } from './prenotazioneescursione.component';

describe('PrenotazioneescursioneComponent', () => {
  let component: PrenotazioneescursioneComponent;
  let fixture: ComponentFixture<PrenotazioneescursioneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrenotazioneescursioneComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PrenotazioneescursioneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
