import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrenotazioneeventoComponent } from './prenotazioneevento.component';

describe('PrenotazioneeventoComponent', () => {
  let component: PrenotazioneeventoComponent;
  let fixture: ComponentFixture<PrenotazioneeventoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrenotazioneeventoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PrenotazioneeventoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
